﻿using Microsoft.EntityFrameworkCore;
using RazorCRUD.Models.Entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RazorCRUD.Models.Contexto
{
    public class Contexto : DbContext
    {
       
        public Contexto(DbContextOptions<Contexto> option) : base(option)
        {
            // Verifica se o banco existe, senão existir, cria/atualiza as colunas
            Database.EnsureCreated();
        }

        public DbSet<Funcionario> Funcionario { get; set; }

        public DbSet<Setor> Setor { get; set; }

    }

}
