﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RazorCRUD.Models.Entidade
{

    [Table("Setor")]
    public class Setor
    {

        [Display(Description = "Código")]
        public int Id { get; set; }

        [Display(Description = "Nome")]
        public string Nome { get; set; }

        [Display(Description = "Sala")]
        public string Sala { get; set; }

        [Display(Description = "Capacidade")]
        public string Capacidade { get; set; }

    }
}
