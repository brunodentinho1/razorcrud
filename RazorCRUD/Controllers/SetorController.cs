﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using RazorCRUD.Models.Contexto;
using RazorCRUD.Models.Entidade;

namespace RazorCRUD.Controllers
{
    public class SetorController : Controller
    {
        private readonly Contexto _contexto;

        public SetorController(Contexto contexto)
        {
            _contexto = contexto;
        }

        public IActionResult Index()
        {
            var lista = _contexto.Setor.ToList();
            return View(lista);
        }
               

        [HttpGet]
        public IActionResult Details(int Id)
        {
            var setor = _contexto.Setor.Find(Id);
            return View(setor);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var setor = new Setor();
            return View(setor);
        }
            
        [HttpPost]
        public IActionResult Create(Setor Setor)
        {
            
            if (ModelState.IsValid)
            {
                _contexto.Setor.Add(Setor);
                _contexto.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(Setor);
        }

        [HttpGet]
        public IActionResult Edit(int Id)
        {
            var Setor = _contexto.Setor.Find(Id);
            return View(Setor);
        }

        [HttpPost]
        public IActionResult Edit(Setor Setor)
        {
  
            if (ModelState.IsValid)
            {
                _contexto.Setor.Update(Setor);
                _contexto.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                return View(Setor);
            }
        }

        [HttpGet]
        public IActionResult Delete(int Id)
        {
            var Setor = _contexto.Setor.Find(Id);
            return View(Setor);
        }


        [HttpPost]
        public IActionResult Delete(Setor _Setor)
        {
            var Setor = _contexto.Setor.Find(_Setor.Id);
            if (Setor != null)
            {
                _contexto.Setor.Remove(Setor);
                _contexto.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(Setor);
        }
    }
}