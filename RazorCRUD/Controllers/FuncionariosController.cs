﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using RazorCRUD.Models.Contexto;
using RazorCRUD.Models.Entidade;

namespace RazorCRUD.Controllers
{
    public class FuncionariosController : Controller
    {
        /// <summary>
        /// Database context
        /// </summary>
        private readonly Contexto _contexto;
        public FuncionariosController(Contexto contexto)
        {
            _contexto = contexto;
        }

        /// <summary>
        /// Primary method that returns a list of rows in database table "Funcionario"
        /// </summary>
        /// <returns>An object from the type "Funcionario"</returns>
        public IActionResult Index()
        {
            // List the table rows from the database
            var lista = _contexto.Funcionario.ToList();
            return View(lista);
        }

        /// <summary>
        /// Insert the options on dropdown list 
        /// </summary>
        public void CarregaTipoFuncionario()
        {
            var ItensTipoFuncionario = new List<SelectListItem>
            {
                new SelectListItem{ Value ="1", Text ="Gestor"},
                new SelectListItem{ Value ="2", Text ="DBA"},
                new SelectListItem{ Value ="3", Text ="Especialista"},
                new SelectListItem{ Value ="4", Text ="Programador"},
                new SelectListItem{ Value ="5", Text ="Trainee"}
            };
            // Load the items on page
            ViewBag.TiposFuncionario = ItensTipoFuncionario;
        }

        [HttpGet]
        public IActionResult Details(int Id)
        {
            var Funcionario = _contexto.Funcionario.Find(Id);
            CarregaTipoFuncionario();
            return View(Funcionario);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var funcionario = new Funcionario();
            CarregaTipoFuncionario();
            return View(funcionario);
        }

        /// <summary>
        /// Send input informations to the database
        /// </summary>
        /// <param name="Funcionario"></param>
        /// <returns>An object from the type "Funcionario" </returns>
        [HttpPost]
        public IActionResult Create(Funcionario Funcionario)
        {
            // Allow the operation when all fields have a value
            if (ModelState.IsValid)
            {
                _contexto.Funcionario.Add(Funcionario);
                _contexto.SaveChanges();

                return RedirectToAction("Index");
            }
            CarregaTipoFuncionario();
            return View(Funcionario);
        }

        [HttpGet]
        public IActionResult Edit(int Id)
        {
            var Funcionario = _contexto.Funcionario.Find(Id);
            CarregaTipoFuncionario();
            return View(Funcionario);
        }

        /// <summary>
        /// Send the edited informations to the database
        /// </summary>
        /// <param name="Funcionario"></param>
        /// <returns>An object from the type "Funcionario" </returns>
        [HttpPost]
        public IActionResult Edit(Funcionario Funcionario)
        {
            // Allow the operation when all fields have a value
            if (ModelState.IsValid)
            {
                _contexto.Funcionario.Update(Funcionario);
                _contexto.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                CarregaTipoFuncionario();
                return View(Funcionario);
            }
        }

        [HttpGet]
        public IActionResult Delete(int Id)
        {
            var Funcionario = _contexto.Funcionario.Find(Id);
            CarregaTipoFuncionario();
            return View(Funcionario);
        }

        /// <summary>
        /// Send the user information to the database end delete the row
        /// </summary>
        /// <param name="Funcionario"></param>
        /// <returns>An object from the type "Funcionario" </returns>
        [HttpPost]
        public IActionResult Delete(Funcionario _Funcionario)
        {
            var Funcionario = _contexto.Funcionario.Find(_Funcionario.Id);
            if (Funcionario != null)
            {
                _contexto.Funcionario.Remove(Funcionario);
                _contexto.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(Funcionario);
        }
    }
}